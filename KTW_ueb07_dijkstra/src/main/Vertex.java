package main;

import java.util.HashMap;
import java.util.Map;

/** One vertex of the graph, complete with mappings to neighbouring vertices */
public class Vertex implements Comparable<Vertex> {
	public final String name;
	public int dist = Integer.MAX_VALUE; // MAX_VALUE assumed to be infinity
	public Vertex previous = null;
	public final Map<Vertex, Integer> neighbours = new HashMap<>();

	public Vertex(String name) {
		this.name = name;
	}

	String getPath() {
		if (this == this.previous) {
			return this.name;
		} else if (this.previous == null) {
			return this.name + "(unreached)";
		} else {
			return this.previous.getPath() + "->" + this.name + "(" + this.dist + ")";
		}
	}

	public int compareTo(Vertex other) {
		if (dist == other.dist)
			return name.compareTo(other.name);

		return Integer.compare(dist, other.dist);
	}
}