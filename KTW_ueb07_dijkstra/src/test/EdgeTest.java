package test;

import static org.junit.Assert.*;

import org.junit.Test;

import main.Edge;

public class EdgeTest {

	@Test
	public void testEdge() {
		Edge edge = new Edge("vertex1", "vertex2", 5);
		assertEquals("vertex1", edge.v1);
		assertEquals("vertex2", edge.v2);
		assertEquals(5, edge.dist);
	}

}
