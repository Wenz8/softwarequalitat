package test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import main.Edge;
import main.Graph;
import main.Vertex;

public class GraphTest {

	Graph graphResult;

	@Before
	public void before() {
		Edge edge1 = new Edge("v1", "v2", 3);
		Edge edge2 = new Edge("v2", "v3", 5);
		Edge edge3 = new Edge("v3", "v4", 1);

		Edge[] edges = new Edge[3];
		edges[0] = edge1;
		edges[1] = edge2;
		edges[2] = edge3;

		graphResult = new Graph(edges);
	}

	@Test
	public void testDijkstra() {
		boolean dijkstra = graphResult.dijkstra("v1");
		assert (dijkstra);

		boolean dijkstra2 = graphResult.dijkstra("v0");
		assert (!dijkstra2);

		Edge edge1 = new Edge("v1", "v2", 3);
		Edge edge2 = new Edge("v2", "v3", 5);
		Edge edge3 = new Edge("v4", "v5", 1);

		Edge[] edges = new Edge[3];
		edges[0] = edge1;
		edges[1] = edge2;
		edges[2] = edge3;

		graphResult = new Graph(edges);

		boolean dijkstra3 = graphResult.dijkstra("v1");
		assert (dijkstra3);
	}

	@Test
	public void testGetPath() {
		String path = graphResult.getPath("v4");
		String expectedPath = "v4(unreached)";
		assertEquals(expectedPath, path);

		boolean dijkstra = graphResult.dijkstra("v1");

		String path2 = graphResult.getPath("v4");
		String expectedPath2 = "v1->v2(3)->v3(8)->v4(9)";
		assertEquals(expectedPath2, path2);

		String path3 = graphResult.getPath("v1");
		String expectedPath3 = "v1";
		assertEquals(expectedPath3, path3);

		String path4 = graphResult.getPath("v0");
		String expectedPath4 = "Graph doesn't contain end vertex \"" + "v0" + "\"";
		assertEquals(expectedPath4, path4);
	}

	@Test
	public void testGetAllPath() {
		String allPaths1 = graphResult.getAllPaths();
		String expectedAllPaths1 = "v1(unreached),v2(unreached),v3(unreached),v4(unreached)";
		assertEquals(expectedAllPaths1, allPaths1);

		boolean dijkstra = graphResult.dijkstra("v1");

		String allPaths2 = graphResult.getAllPaths();
		String expectedAllPaths2 = "v1,v1->v2(3),v1->v2(3)->v3(8),v1->v2(3)->v3(8)->v4(9)";
		assertEquals(expectedAllPaths2, allPaths2);

	}

}
