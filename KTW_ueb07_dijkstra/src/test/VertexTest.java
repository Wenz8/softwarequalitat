package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import main.Vertex;

public class VertexTest {
	Vertex vertex;

	@Before
	public void before() {
		vertex = new Vertex("vertex1");
	}

	@Test
	public void testVertex() {
		assertEquals("vertex1", vertex.name);
		assertEquals(Integer.MAX_VALUE, vertex.dist);
		assertEquals(null, vertex.previous);
		assert (vertex.neighbours.isEmpty());
	}

	@Test
	public void testCompareTo() {
		Vertex other = new Vertex("vertex2");
		int compareToResult = vertex.compareTo(other);
		assert (compareToResult < 0);

		other.dist = 5;
		int compareToResult2 = vertex.compareTo(other);
		assertEquals(1, compareToResult2);
	}

}
