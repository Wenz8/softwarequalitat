import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

// Snippet ID: 366
class FileLineReader {
	public static String[] readAllLines(File file) {

		ArrayList<String> lines = new ArrayList<String>();

		try {

			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = null;

			while ((line = br.readLine()) != null) {
				lines.add(line);
			}

			br.close();
		} catch (IOException e) {
			return null;
		}

		return lines.toArray(new String[lines.size()]);
	}
}