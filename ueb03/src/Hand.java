import java.util.ArrayList;
import java.util.List;

public class Hand {
	private String highestCard;
	private String compareCard;

	private List<String> valueList = new ArrayList<String>();
	private List<String> suitList = new ArrayList<String>();

	public Hand(String hand) {
		String[] cards = hand.split(" ");
		for (String card : cards) {
			if (card.length() > 1) {
				String value = card.charAt(0) + "";
				String suit = card.charAt(1) + "";
				valueList.add(value);
				suitList.add(suit);
			}
		}
	}

	public String getCompareCard() {
		return compareCard;
	}

	public void setCompareCard(String compareCard) {
		this.compareCard = compareCard;
	}

	public String getHighestCard() {
		return highestCard;
	}

	public void setHighestCard(String highestCard) {
		this.highestCard = highestCard;
	}

	public List<String> getValues() {
		return valueList;
	}

	public List<String> getSuits() {
		return suitList;
	}

}
