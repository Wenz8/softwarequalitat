
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class Poker {
	private static String path = "C:\\Users\\viola\\Documents\\Informatik\\Uni\\Softwarequalit�t\\Zettel\\Zettel3\\p054_poker.txt";

	public static void main(String[] args) {
		if (args.length > 0) {
			path = args[0];
		}
		System.out.println("Player 1 wins " + readHands() + " hands.");
	}

	private static int readHands() {
		int wins = 0;
		File file = new File(path);
		String[] lines = FileLineReader.readAllLines(file);
		for (String line : lines) {
			String[] hands = line.split(" ");
			String a = "";
			String b = "";
			int counter = 0;
			for (String hand : hands) {
				if (counter < 5) {
					a = a + hand + " ";
				} else {
					b = b + hand + " ";
				}
				counter = counter + 1;
			}
			Hand pokerHand1 = new Hand(a.substring(0, a.length() - 1));
			Hand pokerHand2 = new Hand(b.substring(0, b.length() - 1));
			wins = wins + compareHands(pokerHand1, pokerHand2);
		}
		return wins;
	}

	private static int compareHands(final Hand pokerHand1, final Hand pokerHand2) {
		int win = 0;
		int rank1 = getRank(pokerHand1);
		int rank2 = getRank(pokerHand2);

		if (rank1 > rank2) {
			win = 1;
		} else if (rank1 == rank2) {
			String compare1 = pokerHand1.getCompareCard();
			String compare2 = pokerHand2.getCompareCard();
			int c1 = stringToInt(compare1);
			int c2 = stringToInt(compare2);

			String high1 = pokerHand1.getHighestCard();
			String high2 = pokerHand2.getHighestCard();
			int h1 = stringToInt(high1);
			int h2 = stringToInt(high2);
			if (c1 > c2) {
				win = 1;
			} else if (c1 == c2) {
				if (h1 > h2) {
					win = 1;
				}
			}
		}
		return win;
	}

	private static int stringToInt(String high) {
		if (high.equals("A")) {
			return 14;
		} else if (high.equals("K")) {
			return 13;
		} else if (high.equals("Q")) {
			return 12;
		} else if (high.equals("J")) {
			return 11;
		} else if (high.equals("T")) {
			return 10;
		} else {
			return Integer.parseInt(high);
		}
	}

	private static int getRank(Hand pokerHand) {
		List<String> suits = pokerHand.getSuits();
		List<String> values = pokerHand.getValues();

		Boolean suitsEqual = true;
		String suit1 = suits.get(0);
		for (String suit : suits) {
			if (!suit.equals(suit1)) {
				suitsEqual = false;
			}
		}

		Boolean royalFlush = royalFlush(values, suitsEqual);

		HashMap<String, Integer> map = new HashMap<String, Integer>();
		for (String value : values) {
			map.put(value, suitsEqual(values, value));
		}
		HashSet<String> fourEqualValues = valueEquals(map, 4);
		HashSet<String> threeEqualValues = valueEquals(map, 3);
		HashSet<String> twoEqualValues = valueEquals(map, 2);

		Boolean allConsec = consecValues(values, "9", "T", "J", "Q", "K")
				|| consecValues(values, "A", "T", "J", "Q", "K") || consecValues(values, "9", "T", "J", "Q", "8")
				|| consecValues(values, "9", "T", "J", "7", "8") || consecValues(values, "9", "T", "6", "7", "8")
				|| consecValues(values, "9", "5", "6", "7", "8") || consecValues(values, "4", "5", "6", "7", "8")
				|| consecValues(values, "4", "5", "6", "7", "3") || consecValues(values, "4", "5", "6", "2", "3")
				|| consecValues(values, "4", "5", "A", "2", "3");

		String highestCard = getHighestValue(values);
		pokerHand.setHighestCard(highestCard);

		if (royalFlush) {
			pokerHand.setCompareCard("A");
			return 10;
		} else if (allConsec && suitsEqual) {
			pokerHand.setCompareCard(highestCard);
			return 9;
		} else if (!fourEqualValues.isEmpty()) {
			setCompareCard(pokerHand, fourEqualValues);
			return 8;
		} else if (!threeEqualValues.isEmpty() && twoEqualValues.size() > 1) {
			setCompareCard(pokerHand, twoEqualValues);
			return 7;
		} else if (suitsEqual) {
			pokerHand.setCompareCard(highestCard);
			return 6;
		} else if (allConsec) {
			pokerHand.setCompareCard(highestCard);
			return 5;
		} else if (!threeEqualValues.isEmpty()) {
			setCompareCard(pokerHand, threeEqualValues);
			return 4;
		} else if (twoEqualValues.size() > 1) {
			setCompareCard(pokerHand, twoEqualValues);
			return 3;
		} else if (!twoEqualValues.isEmpty()) {
			setCompareCard(pokerHand, twoEqualValues);
			return 2;
		} else {
			pokerHand.setCompareCard(highestCard);
			return 1;
		}
	}

	private static void setCompareCard(Hand pokerHand, HashSet<String> equalValues) {
		String[] ar = equalValues.toArray(new String[equalValues.size()]);
		String c1 = ar[0];
		for (String a : ar) {
			if (stringToInt(a) > stringToInt(c1)) {
				c1 = a;
			}
		}
		pokerHand.setCompareCard(c1);
	}

	private static Boolean royalFlush(List<String> values, Boolean suitsEqual) {
		Boolean royalFlush = false;
		HashSet<String> royalFlushValues = new HashSet<String>();
		royalFlushValues.add("T");
		royalFlushValues.add("J");
		royalFlushValues.add("Q");
		royalFlushValues.add("K");
		royalFlushValues.add("A");
		if (values.containsAll(royalFlushValues) && suitsEqual) {
			royalFlush = true;
		}
		return royalFlush;
	}

	private static Boolean consecValues(List<String> values, final String a, final String b, final String c,
			final String d, final String e) {
		Boolean allConsec = false;
		HashSet<String> consecValues = new HashSet<String>();
		consecValues.add(a);
		consecValues.add(b);
		consecValues.add(c);
		consecValues.add(d);
		consecValues.add(e);
		if (values.containsAll(consecValues)) {
			allConsec = true;
		}
		return allConsec;
	}

	private static String getHighestValue(final List<String> values) {
		String highestValue = "";
		if (values.contains("A")) {
			highestValue = "A";
		} else if (values.contains("K")) {
			highestValue = "K";
		} else if (values.contains("Q")) {
			highestValue = "Q";
		} else if (values.contains("J")) {
			highestValue = "J";
		} else if (values.contains("T")) {
			highestValue = "T";
		} else if (values.contains("9")) {
			highestValue = "9";
		} else if (values.contains("8")) {
			highestValue = "8";
		} else if (values.contains("7")) {
			highestValue = "7";
		} else if (values.contains("6")) {
			highestValue = "6";
		} else if (values.contains("5")) {
			highestValue = "5";
		} else if (values.contains("4")) {
			highestValue = "4";
		} else if (values.contains("3")) {
			highestValue = "3";
		} else {
			highestValue = "2";
		}
		return highestValue;
	}

	private static HashSet<String> valueEquals(final HashMap<String, Integer> map, int x) {
		HashSet<String> keySet = new HashSet<String>();
		for (String key : map.keySet()) {
			if (map.get(key) >= x) {
				keySet.add(key);
			}
		}
		return keySet;
	}

	private static int suitsEqual(final List<String> values, String value1) {
		int suitsEqualCount = 0;
		for (String value : values) {
			if (value.equals(value1)) {
				suitsEqualCount = suitsEqualCount + 1;
			}
		}
		return suitsEqualCount;
	}

}
