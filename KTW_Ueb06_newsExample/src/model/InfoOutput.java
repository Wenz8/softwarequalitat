package model;

import java.util.ArrayList;
import java.util.List;

public class InfoOutput implements Output {

	@Override
	public void print(Article article) {
		System.out.println("Article: " + article.getTitle() + ", Author: " + article.getAuthor());
	}

	@Override
	public void print(Image image) {
		System.out.println("Image: " + image.getTitle() + ", Resolution: " + image.getWidth() + "x" + image.getHeight()
				+ ", Author: " + image.getAuthor());

	}

	@Override
	public void print(NewsCollection newsCollection) {
		System.out.println("###" + newsCollection.getTopic() + "###");
		List<News> allNews = new ArrayList<News>();
		allNews.addAll(newsCollection.getArticles());
		allNews.addAll(newsCollection.getImages());
		allNews.addAll(newsCollection.getCollections());
		for(News newsElement : allNews){
			newsElement.print(this);
		}

	}

}
