package model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class NewsCollection implements News{
	
	private String topic;
//	private List<Article> articles = new LinkedList<Article>();
//	private List<Image> images = new LinkedList<Image>();
//	private List<NewsCollection> collections = new LinkedList<NewsCollection>();
	private List<News> news = new LinkedList<News>();

		
	public NewsCollection(String topic) {
		this.topic = topic;
	}
	
	/**
	 * Get the topic of the collection.
	 * @return
	 */
	public String getTopic() {
		return topic;
	}
	/**
	 * Get the list of articles stored directly in this collection.
	 * @return articles stored in this collection
	 */
	public List<Article> getArticles() {
		List<Article> articles = new ArrayList<Article>();
		for(News newsElement : news){
			if(newsElement instanceof Article){
				articles.add((Article) newsElement);
			}
		}
		return articles;
	}
	
	/**
	 * Get the list of images stored directly in this collection.
	 * @return images stored in this collection
	 */
	public List<Image> getImages() {
		List<Image> images = new ArrayList<Image>();
		for(News newsElement : news){
			if(newsElement instanceof Image){
				images.add((Image) newsElement);
			}
		}
		return images;
	}
	
	/**
	 * Get the list of collections stored directly in this collection.
	 * @return collections stored in this collection
	 */
	public List<NewsCollection> getCollections() {
		List<NewsCollection> collections = new ArrayList<NewsCollection>();
		for(News newsElement : news){
			if(newsElement instanceof NewsCollection){
				collections.add((NewsCollection) newsElement);
			}
		}
		return collections;
	}
	
	/**
	 * Store an article directly in this collection.
	 * @param article
	 * @return this collection (enables method chaining)
	 */
	public NewsCollection addArticle(Article article) {
		news.add(article);
		return this;
	}
	
	/**
	 * Store an image directly in this collection.
	 * @param image
	 * @return this collection (enables method chaining)
	 */
	public NewsCollection addImage(Image image) {
		news.add(image);
		return this;
	}
		
	/**
	 * Store another collection directly in this collection.
	 * @param collection
	 * @return this collection (enables method chaining)
	 */
	public NewsCollection addCollection(NewsCollection collection) {
		news.add(collection);
		return this;
	}
	
	@Override
	public void print(Output output) {
		output.print(this);		
		
//		for(News newsElement : news){
//			newsElement.print(output);
//		}
		
	}
}
