package model;

import java.util.ArrayList;
import java.util.List;

public class ContentOutput implements Output{

	@Override
	public void print(Article article) {
		System.out.println("###" + article.getTitle() + "###");
		System.out.println(article.getContent());
		System.out.println();		
	}

	@Override
	public void print(Image image) {
		System.out.println("###" + image.getTitle() + "###");
		image.drawImage();
		System.out.println();		
	}

	@Override
	public void print(NewsCollection newsCollection) {
		List<News> allNews = new ArrayList<News>();
		allNews.addAll(newsCollection.getArticles());
		allNews.addAll(newsCollection.getImages());
		allNews.addAll(newsCollection.getCollections());
		for(News newsElement : allNews){	
			newsElement.print(this);
		}
		
	}

}
