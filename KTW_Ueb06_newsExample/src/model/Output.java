package model;

public interface Output {
	public void print(Article article);
	public void print(Image image);
	public void print(NewsCollection newsCollection);
}
