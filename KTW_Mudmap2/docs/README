MUD Map, version 2
by Neop (contact: email: mneop@web.de)

license: GPLv3
use it on your own risk!

MUD Map Websites:
GitHub (Sources, Information, etc): https://github.com/Neop/mudmap2
Sourceforge (download packages, MUD Map v1 + v2): https://sourceforge.net/projects/mudmap/
MUD Map v1 user manual / quick start guide: http://mudmap.sf.net

If you encounter any bugs or other problems please contact me via email or write
a bug report on GitHub (https://github.com/Neop/mudmap2/issues), so I
can fix it.

Installation
A JRE (Java Runtime Environment) needs to be installed on your computer.
You can get it here: https://www.java.com/de/download/index.jsp 
Place the mudmap.jar file wherever you like. It should run after a 
double-click on the mudmap.jar file or right click on it, select
"open/run with", then select something siliar to "Java JRE" (It depends
on your operating system)

Portable mode
When you start MUD Map for the first time you will be asked whether
you'd like to use MUD Map in portable mode. In this case the world files
will be stored in the same directory as the mudmap2.jar.

If you don't use portable mode, you can find the files here:
Linux: ~/.mudmap/
Windows: %appdata%\mudmap\

Updating
Just replace the old mudmap2.jar file with a new one.

Compatibility to MUD Map 1
MUD Map 2 uses an improved version of the file format of MUD Map 1. It
can read worlds that were created with MUD Map 1 and saves them in a
compatible format if compatibility is enabled (is enabled by 
default)

How to use MUD Map:
I didn't create a manual for MUD Map 2 yet, so please refer to the 
manual of MUD Map 1, which is very similar: http://mudmap.sf.net
If you still have questions, feel free to contact me ;)

Keyboard commands
Keyboard commands can be used to navigate and alter the map if keyboard
place selection is enabled by pressing p. A red box should be visible on
the screen. You might have to click at the map once after you opened it
for it to be able to receive keyboard events.

The keys used here are not final and might not be optimal. Please let me
know if you think that other keys should be used ;)

ctrl + s							save currently shown world
ctrl + o							show open world dialog

context menu key			show the context menu of the selected
									place (like right click on that place)

+/- 		page up/down		increment and decrement the tile size
p:									enable / disable place selection

w/a/s/d, arrow keys, numberpad	shift the selection ("direction keys")
h, home key, numpad 5		go to home location
e, enter, insert key, double click	create / edit place at selection
f									create or remove a placeholder place
r 			delete key			remove selected place
c									edit place comments
q									modify place group of selected place. Note that
									this also affects other places that
									belong to the same group. If no place is
									selected or selection is disabled, a new
									place group will be created
l									show place list

path creation shortcuts:
ctrl + numpad 5					show create path dialog
ctrl + direction key				create path to adjacent place 
(no w/a/s/d)
alt or alt-gr + direction key	remove all paths to adjacent place

place selection (to remove / move / copy many places at once):
escape							deselect everything
space							add / remove place selected by cursor
ctrl + click						add / remove place to selection
ctrl + a							select everything on current layer
shift + click						box selection
shift + arrow keys / numpad / w/a/s/d	box selection

ctrl + x							cut places
ctrl + c							copy places
ctrl + v							paste places
