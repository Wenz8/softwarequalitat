package mudmap2.frontend.GUIElement.WorldPanel;

import java.awt.Graphics;

public class Graphic {
	public Graphics g;
	public int x;
	public int y;
	public int width;
	public int height;

	public Graphic(Graphics g, int x, int y, int width, int height) {
		this.g = g;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
}