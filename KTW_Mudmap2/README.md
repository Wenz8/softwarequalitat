#MUD Map v2 (mudmap2)

A mapping tool for text-based games, like text adventures, MUDs and MUSHs.
An improved and compatible rewrite of MUD Map v1

license: GPLv3  
use it on your own risk!

##MUD Map Websites:
**GitHub** (Sources, Information, etc): https://github.com/Neop/mudmap2  
**Sourceforge** (download packages, MUD Map v1 + v2): https://sourceforge.net/projects/mudmap/  
MUD Map v1 user manual / **quick start guide**: http://mudmap.sf.net

Developer contact details can be found in doc/README.

If you encounter any bugs or other problems please contact me or write
a bug report on GitHub (https://github.com/Neop/mudmap2/issues), so I
can fix it.

##Installation
A JRE (Java Runtime Environment) needs to be installed on your computer.
You can get it here: https://www.java.com/de/download/index.jsp 
Place the mudmap.jar file wherever you like. It should run after a 
double-click on the mudmap.jar file or right click on it, select
"open/run with", then select something siliar to "Java JRE" (It depends
on your operating system)

Portable mode
When you start MUD Map for the first time you will be asked whether
you'd like to use MUD Map in portable mode. In this case the world files
will be stored in the same directory as the mudmap2.jar.

If you don't use portable mode, you can find the files here:  
Linux: ~/.mudmap/  
Windows: %appdata%\mudmap\  

##Updating
Just replace the old mudmap2.jar file with a new one.

##Compatibility to MUD Map 1
MUD Map 2 uses an improved version of the file format of MUD Map 1. It
can read worlds that were created with MUD Map 1 and saves them in a
compatible format if compatibility is enabled (is enabled by 
default) Note that MUD Map 1 might be updated to use the same file
format after version 1.4

##How to use MUD Map:
I didn't create a manual for MUD Map 2 yet, so please refer to the 
manual of MUD Map 1, which is very similar: http://mudmap.sf.net
If you've still questions, feel free to contact me ;)

###Keyboard commands
Keyboard commands can be used to navigate and alter the map if keyboard
place selection is enabled by pressing p. A red box should be visible on
the screen. You might have to click at the map once after you opened it.

The keys used here are not final and might not be optimal. Please let me
know if you think that other keys should be used ;)

You can find the key list in doc/README.
