package org.mafagafogigante.dungeon.entity.items;

import org.mafagafogigante.dungeon.entity.Integrity;
import org.mafagafogigante.dungeon.entity.Preset;
import org.mafagafogigante.dungeon.entity.TagSet;
import org.mafagafogigante.dungeon.entity.creatures.Creature;
import org.mafagafogigante.dungeon.entity.creatures.Creature.Tag;
import org.mafagafogigante.dungeon.game.Id;
import org.mafagafogigante.dungeon.util.Percentage;

import java.io.Serializable;

/**
 * Stores the information about an item that the factory may need to create it.
 */
public final class ItemPreset extends Preset implements Serializable {

  private static final long serialVersionUID = -3795444867261167258L;
  private Integrity integrity;
  private int damage;
  private Percentage hitRate;
  private int integrityDecrementOnHit;
  private int nutrition;
  private int integrityDecrementOnEat;
  private Id spellId;
  private String text;
  private long putrefactionPeriod;
  private boolean unique;
  private int drinkableHealing;
  private int drinkableDoses;
  private int integrityDecrementPerDose;
  protected TagSet<Item.Tag> tagSet = TagSet.makeEmptyTagSet(Item.Tag.class);

  public TagSet<Item.Tag> getTagSet() {
    return tagSet;
  }

  public boolean hasTag(Item.Tag tag) {
    return getTagSet().hasTag(tag);
  }

  public void addTag(Item.Tag tag) {
    getTagSet().addTag(tag);
  }

  public Integrity getIntegrity() {
    return integrity;
  }

  public void setIntegrity(Integrity integrity) {
    this.integrity = integrity;
  }

  public int getDamage() {
    return damage;
  }

  public void setDamage(int damage) {
    this.damage = damage;
  }

  public Percentage getHitRate() {
    return hitRate;
  }

  public void setHitRate(Percentage hitRate) {
    this.hitRate = hitRate;
  }

  public int getIntegrityDecrementOnHit() {
    return integrityDecrementOnHit;
  }

  public void setIntegrityDecrementOnHit(int integrityDecrementOnHit) {
    this.integrityDecrementOnHit = integrityDecrementOnHit;
  }

  public int getNutrition() {
    return nutrition;
  }

  public void setNutrition(int nutrition) {
    this.nutrition = nutrition;
  }

  public int getIntegrityDecrementOnEat() {
    return integrityDecrementOnEat;
  }

  public void setIntegrityDecrementOnEat(int integrityDecrementOnEat) {
    this.integrityDecrementOnEat = integrityDecrementOnEat;
  }

  public Id getSpellId() {
    return spellId;
  }

  public void setSpellId(String spellIdString) {
    this.spellId = new Id(spellIdString);
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public long getPutrefactionPeriod() {
    return putrefactionPeriod;
  }

  public void setPutrefactionPeriod(long putrefactionPeriod) {
    this.putrefactionPeriod = putrefactionPeriod;
  }

  public boolean isUnique() {
    return unique;
  }

  public void setUnique(boolean unique) {
    this.unique = unique;
  }

  public int getIntegrityDecrementPerDose() {
    return integrityDecrementPerDose;
  }

  public void setIntegrityDecrementPerDose(int integrityDecrementPerDose) {
    this.integrityDecrementPerDose = integrityDecrementPerDose;
  }

  public int getDrinkableHealing() {
    return drinkableHealing;
  }

  public void setDrinkableHealing(int healing) {
    this.drinkableHealing = healing;
  }

  public int getDrinkableDoses() {
    return drinkableDoses;
  }

  public void setDrinkableDoses(int doses) {
    this.drinkableDoses = doses;
  }

}
