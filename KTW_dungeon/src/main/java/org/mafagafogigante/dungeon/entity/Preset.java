package org.mafagafogigante.dungeon.entity;

import org.mafagafogigante.dungeon.game.Id;
import org.mafagafogigante.dungeon.game.Name;
import org.mafagafogigante.dungeon.util.Percentage;

/**
 * An interface that simplifies Entity instantiation.
 */
public abstract class Preset {

	protected Id id;
	protected String type;
	protected Name name;
	protected Weight weight;
	protected Percentage visibility;
	protected Luminosity luminosity = Luminosity.ZERO;
	public Id getId() {
	    return id;
	  }
	public void setId(Id id) {
	    this.id = id;
	  }
	public String getType() {
	    return type;
	  }
	public void setType(String type) {
	    this.type = type;
	  }
	public Name getName() {
	    return name;
	  }
	public void setName(Name name) {
	    this.name = name;
	  }
	public Weight getWeight() {
	    return weight;
	  }
	public void setWeight(Weight weight) {
	    this.weight = weight;
	  }
	public Percentage getVisibility() {
	    return visibility;
	  }
	public void setVisibility(Percentage visibility) {
	    this.visibility = visibility;
	  }
	public Luminosity getLuminosity() {
	    return luminosity;
	  }
	public void setLuminosity(Luminosity luminosity) {
	    this.luminosity = luminosity;
	  }

//Id getId();
//
//  String getType();
//
//  Name getName();
//
//  Weight getWeight();
//
//  Percentage getVisibility();

}
